import { default as AbstractApiService, RequestService } from '@jclib/api-service';
import { LocalStorageService, SessionStorageService }    from '@jclib/storage-service';
import { dispatch }                                      from '@jclib/react-redux-store';

import { default as ResponseService } from './ResponseService';

class ApiService extends AbstractApiService {
  dispatch;
  ls;
  ss;

  constructor(urlKey = '', prefix = '') {
    super({baseUrl: '/api/', formData: true, urlKey});

    this.dispatch = dispatch;
    this.ls       = new LocalStorageService(prefix);
    this.res      = new ResponseService();
    this.ss       = new SessionStorageService(prefix);
  }

  get = (url, callback, options = {}) => {
    return this._processResponse(this.req.get(this._getUrl(url), {
      ...options,
      headers:         {
        ...(options.headers || {}),
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type':     'application/json',
      },
      withCredentials: true,
    }), callback);
  };

  post = (url, data, callback, options = {}) => {
    return this._processResponse(this.req.post(this._getUrl(url), data, {
      ...options,
      headers:         {
        ...(options.headers || {}),
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type':     'multipart/form-data',
      },
      withCredentials: true,
    }), callback);
  };
}

export default ApiService;
export { RequestService, ResponseService };