import { ResponseService as AbstractResponseService } from '@jclib/api-service';

const errorConstants = {
  SERVER_ERROR:         'SERVER_ERROR',
  COMMON_ERROR:         'COMMON_ERROR',
  VALIDATION_ERROR:     'VALIDATION_ERROR',
  WRONG_RESPONSE_ERROR: 'WRONG_RESPONSE_ERROR',
  NOT_FOUND_ERROR:      'NOT_FOUND_ERROR',
  NOT_AUTHORIZED_ERROR: 'NOT_AUTHORIZED_ERROR',
};

class ResponseService extends AbstractResponseService {
  processResponse = (promise) => {
    return promise
      .then(response => {
        if (200 === response.status) {
          const {success, errors, vars, messageType, message} = response.data;

          if (
            typeof success === 'undefined' ||
            typeof errors === 'undefined' ||
            typeof vars === 'undefined' ||
            typeof messageType === 'undefined' ||
            typeof message === 'undefined'
          ) {
            return Promise.reject({errorType: errorConstants.WRONG_RESPONSE_ERROR});
          }

          if (true === success) {
            return response;
          }

          if (errors.common) {
            return Promise.reject({errorType: errorConstants.COMMON_ERROR, messageType, message: errors.common});
          }

          if (Object.keys(errors.validation).length) {
            return Promise.reject({errorType: errorConstants.VALIDATION_ERROR, messageType, errors: errors.validation});
          }
        }

        throw Error;
      })
      .catch(error => {
        if (error instanceof Error) {
          switch (error.message) {
            case 'Request failed with status code 404':
              return Promise.reject({errorType: errorConstants.NOT_FOUND_ERROR});
            case 'Request failed with status code 403':
              return Promise.reject({errorType: errorConstants.NOT_AUTHORIZED_ERROR});
            case 'Request aborted':
              break;
            default:
              return Promise.reject({errorType: errorConstants.SERVER_ERROR});
          }
        }

        return Promise.reject(error);
      });
  };

  doFailureAction = (error) => {
    switch (error.errorType) {
      case errorConstants.SERVER_ERROR:
        return Promise.reject({
          messageType: 'error',
          message:     'Произошла ошибка при выполнении запроса',
        });

      case errorConstants.WRONG_RESPONSE_ERROR:
        return Promise.reject({messageType: 'error', message: 'Некорректный формат ответа'});

      case errorConstants.NOT_FOUND_ERROR:
        return Promise.reject({messageType: 'warning', message: 'Не найден обработчик запроса'});

      case errorConstants.NOT_AUTHORIZED_ERROR:
        return Promise.reject({messageType: 'warning', message: 'Отказано в доступе'});

      case errorConstants.VALIDATION_ERROR:
        return Promise.reject(error.errors);

      default:
        return Promise.reject(error);
    }
  };

  doSuccessAction = (response) => {
    if (response.data.message) {
      return Promise.reject({
        messageType: 'success',
        message:     response.data.message,
      });
    }

    return response.data.vars;
  };
}

export default ResponseService;