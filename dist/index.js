"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "RequestService", {
  enumerable: true,
  get: function get() {
    return _apiService.RequestService;
  }
});
Object.defineProperty(exports, "ResponseService", {
  enumerable: true,
  get: function get() {
    return _ResponseService.default;
  }
});
exports.default = void 0;

var _apiService = _interopRequireWildcard(require("@jclib/api-service"));

var _storageService = require("@jclib/storage-service");

var _reactReduxStore = require("@jclib/react-redux-store");

var _ResponseService = _interopRequireDefault(require("./ResponseService"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class ApiService extends _apiService.default {
  constructor() {
    var _this;

    var urlKey = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    var prefix = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
    super({
      baseUrl: '/api/',
      formData: true,
      urlKey
    });
    _this = this;

    _defineProperty(this, "dispatch", void 0);

    _defineProperty(this, "ls", void 0);

    _defineProperty(this, "ss", void 0);

    _defineProperty(this, "get", function (url, callback) {
      var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
      return _this._processResponse(_this.req.get(_this._getUrl(url), _objectSpread(_objectSpread({}, options), {}, {
        headers: _objectSpread(_objectSpread({}, options.headers || {}), {}, {
          'X-Requested-With': 'XMLHttpRequest',
          'Content-Type': 'application/json'
        }),
        withCredentials: true
      })), callback);
    });

    _defineProperty(this, "post", function (url, data, callback) {
      var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
      return _this._processResponse(_this.req.post(_this._getUrl(url), data, _objectSpread(_objectSpread({}, options), {}, {
        headers: _objectSpread(_objectSpread({}, options.headers || {}), {}, {
          'X-Requested-With': 'XMLHttpRequest',
          'Content-Type': 'multipart/form-data'
        }),
        withCredentials: true
      })), callback);
    });

    this.dispatch = _reactReduxStore.dispatch;
    this.ls = new _storageService.LocalStorageService(prefix);
    this.res = new _ResponseService.default();
    this.ss = new _storageService.SessionStorageService(prefix);
  }

}

var _default = ApiService;
exports.default = _default;